#include <iostream>
#include <ctime> //for srand(time) seed

void PrintIntroduction(int Difficulty)
{
    //some text about game themes.
    std::cout << "\n \n You need to steal data files from level: " << Difficulty << " , ";
    std::cout << "so be ready... \n |Enter correct code to continue| \n\n";
}

bool PlayGame(int Difficulty)
{
    PrintIntroduction(Difficulty);

    const int CodeA = rand() % Difficulty + Difficulty;
    const int CodeB = rand() % Difficulty + Difficulty;
    const int CodeC = rand() % Difficulty + Difficulty;

    const int CodeSum = CodeA + CodeB + CodeC;
    const int CodeProduct = CodeA * CodeB * CodeC;

    std::cout << "There 3 numbers in code\n The codes add-up to: " << CodeSum;
    std::cout << "\n The codes multiply to give: " << CodeProduct << '\n';

    int GuessA, GuessB, GuessC;
    std::cin >> GuessA >> GuessB >> GuessC;

    int GuessSum = GuessA + GuessB + GuessC;
    int GuessProduct = GuessA * GuessB * GuessC;

    if (GuessSum == CodeSum && GuessProduct == CodeProduct)
    {
        //complete level
        std::cout << "\n *** Good job, now - let's start next file! ***";
        return true;
    }
    else 
    {
        //lose at level, just restart from same level.
        std::cout << "\n *** Wrong code, please try again! ***";
        return false;
    }
}

int main()
{
    srand(time(NULL)); //change "random" seed

    int LevelDifficulty = 1;
    int const MaxDifficulty = 5;

    while (LevelDifficulty <= MaxDifficulty)
    {
        bool bLevelComplete = PlayGame(LevelDifficulty);
        // to stop some problems with char in answers
        std::cin.clear();
        std::cin.ignore();

        if (bLevelComplete)
        {
            ++LevelDifficulty;
        }

    }
    std::cout << "\n *** Good job,you've got every data file! Now get out of there! ***\n";
    return 0;
}
